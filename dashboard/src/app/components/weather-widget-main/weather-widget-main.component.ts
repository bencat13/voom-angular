import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-weather-widget-main',
  templateUrl: './weather-widget-main.component.html',
  styleUrls: ['./weather-widget-main.component.css']
})
export class WeatherWidgetMainComponent implements OnInit {

  @Input() public lat;
  @Input() public lng;
  
  public icon='http://openweathermap.org/img/w/10d.png';
  WeatherData:any;
  constructor() { }

  ngOnInit() {
    this.WeatherData = {
      main : {},
      isDay: true
    };
   
    this.getWeatherData(this.lat,this.lng);
  }

   //Calculates weather at [google-map] latiitude and longitude
  getWeatherData(lat,lng){

    let startFrom = new Date().getTime();     
    let string = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lng + "&appid=2c24b3ceeea6dee8fc730af0b86b540a"


   
    fetch(string)
    .then(response=>response.json())
    .then(data=>{this.setWeatherData(data,startFrom);})
  }

  setWeatherData(data,startFrom){
    let queryTime = new Date().getTime() - startFrom
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;

    this.WeatherData = data;
    this.WeatherData.queryTime = "Response Time: " + queryTime + "ms"
    this.WeatherData.isClear = data.weather[0].main == "Clear"
    this.WeatherData.isDay = data.weather[0].icon.indexOf("d")>-1;
    this.WeatherData.temp_celcius = (this.WeatherData.main.temp - 273.15).toFixed(0);
    this.WeatherData.temp_min = (this.WeatherData.main.temp_min - 273.15).toFixed(0);
    this.WeatherData.temp_max = (this.WeatherData.main.temp_max - 273.15).toFixed(0);
    this.WeatherData.temp_feels_like = (this.WeatherData.main.feels_like - 273.15).toFixed(0);
    this.icon='http://openweathermap.org/img/wn/' + data.weather[0].icon + '@2x.png';
    (<HTMLImageElement>document.querySelector(".weather-icon")).src=this.icon

    document.querySelector("#weather-date-time").innerHTML="Date: " + dateTime
    
    

  }

}



