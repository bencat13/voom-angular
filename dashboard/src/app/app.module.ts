import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { GoogleMapsModule } from '@angular/google-maps'
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { WidgetContainerComponent } from './components/widget-container/widget-container.component';
import { WeatherWidgetMainComponent } from './components/weather-widget-main/weather-widget-main.component';
import { FormComponent } from './components/form/form.component';
import { InstructionsComponent } from './components/instructions/instructions.component';
import { QuoteWidgetComponent } from './components/quote-widget/quote-widget.component';
import { HttpClientModule } from '@angular/common/http';
import { CalculatorComponent } from './components/calculator/calculator.component';

@NgModule({ 
  declarations: [AppComponent, HeaderComponent, WidgetContainerComponent, WeatherWidgetMainComponent, FormComponent, InstructionsComponent, QuoteWidgetComponent, CalculatorComponent],
  imports: [BrowserModule, GoogleMapsModule,HttpClientModule],

  providers: [],
  bootstrap: [AppComponent],
})


export class AppModule {}
