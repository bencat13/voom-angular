import { Component, OnInit, ViewChild} from '@angular/core';
import { WeatherWidgetMainComponent } from '../weather-widget-main/weather-widget-main.component';
@Component({
  selector: 'app-widget-container',
  templateUrl: './widget-container.component.html',
  styleUrls: ['./widget-container.component.css']
})
export class WidgetContainerComponent implements OnInit {
  @ViewChild(WeatherWidgetMainComponent ) child: WeatherWidgetMainComponent ;

  lat = +32.0853;
  lng = +34.7818;
  //Updates [lat], [lng], [google-map] marker, and [WeatherWidgetMainComponent]
  onChoseLocation(event) {
    this.lng= event.latLng.lng();
    this.lat= event.latLng.lat();  
    document.querySelector("#lat-map").innerHTML="" + this.lat
    document.querySelector("#lng-map").innerHTML="" + this.lng
    this.child.getWeatherData(this.lat,this.lng);
  }
  
  ngOnInit(): void {
  }
}
