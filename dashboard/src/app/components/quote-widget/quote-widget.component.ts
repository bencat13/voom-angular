import { Component, OnInit } from '@angular/core';
import {Quote} from "../../../models/quote.class"
@Component({
  selector: 'app-quote-widget',
  templateUrl: './quote-widget.component.html',
  styleUrls: ['./quote-widget.component.css']
})
export class QuoteWidgetComponent implements OnInit {
  public quote=new Quote('','');
  constructor() { 
  this.getQuote()
  }

  ngOnInit(): void {
  }


  public getQuote(){
    let startFrom = new Date().getTime();  
    var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var dateTime = date+' '+time;


      fetch('https://api.quotable.io/random')
.then(response => response.json())
.then(data => {
  let queryTime = new Date().getTime() - startFrom
  this.quote=new Quote(data.content,data.author);
  (document.querySelector(".quote")).innerHTML='"' + this.quote.quote +'"';
  (document.querySelector(".author")).innerHTML=this.quote.author;
  (document.querySelector("#quoteQueryTime")).innerHTML="Response Time: " + queryTime+"ms";
  (document.querySelector("#quoteTimeStamp")).innerHTML="Date: " + dateTime;
  
  
})
  }
}
