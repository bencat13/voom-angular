## To Run

1. cd into dashboard
2. run ng serve 
3. Go to localhost: 4200 in web browser

## Purpose

This insurance calculator can be utilized by users to determine per mile insurance rates based on realtime environmental data and the user's personal information.

## Important Notes

Limited resarch was done on how vehicle insurance is determined, and therefore much of the information used in calculating the rates may be irrelevant and inaccurate

## Resources Used

**APIs**

1. Google Maps API - Displays interactable map with latitude and longitude data

2. OpenWeatherMap API - Gathers realtime weather data

3. Quotable API - Gnerates random quotes and associated author

**Guides, Code Snippets, and Research**

1. CSS Styling for Quote Button - Aniimated CSS Button - Alexander Bodin - https://webdeasy.de/en/top-css-buttons-en/

2. CSS Styling for Calculator Button - Hilary - https://webdeasy.de/en/top-css-buttons-en/

3. OpenWeatherMap Guide - https://github.com/Nitij/Angular-Weather-Widget

4. Insurance Rates Guide - https://www.buyautoinsurance.com/average-car-insurance-rates-by-age/#:~:text=Drivers%20aged%20between%2016%20and,drivers%20represent%20a%20higher%20risk.

5. Per Mile Average Rates - https://www.forbes.com/advisor/car-insurance/pay-per-mile/#:~:text=The%20average%20rate%20per%20mile,average%20of%20%24741%20a%20year.





