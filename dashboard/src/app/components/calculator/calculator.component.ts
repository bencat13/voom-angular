import { Component, OnInit } from '@angular/core';
declare var require: any
//var bodyParser = require('body-parser');
//var app = express()
//app.use(bodyParser());


@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  //Button animation
  var animateButton = function(e) {
  e.preventDefault;
  //reset animation
  e.target.classList.remove('animate');

  e.target.classList.add('animate');
  setTimeout(function(){
  e.target.classList.remove('animate');
  },300);
  };

var bubblyButtons = document.getElementsByClassName("bubbly-button");

for (var i = 0; i < bubblyButtons.length; i++) {
bubblyButtons[i].addEventListener('click', animateButton, false);
}
  }


  getRate(){
    //Quote Data
    let quoteSubmit= (document.querySelector(".quote")).innerHTML;
    let authorSubmit=(document.querySelector(".author")).innerHTML;
    //Map Data
    let latitudeSubmit=document.querySelector("#lat-map").innerHTML;
    let longitudeSubmit=document.querySelector("#lng-map").innerHTML;
    //Weather Data
    let weatherImageSubmit=(<HTMLImageElement>document.querySelector(".weather-icon")).src 
    let temperatureSubmit=document.querySelector("#temp-weather").innerHTML;
    let highLowSubmit=document.querySelector("#temp-highlow").innerHTML;
    let feelsLikeSubmit=document.querySelector("#temp-feelslike").innerHTML;
    let citySubmit=document.querySelector("#city").innerHTML;
    let humiditySubmit=document.querySelector("#humidity").innerHTML;
    //Form Data
    let genderSubmit = (<HTMLInputElement>document.querySelector('input[name="gender"]:checked')).value;
    let ageSubmit=(<HTMLInputElement>document.querySelector('#age')).value;
    let yearsDrivenSubmit=(<HTMLInputElement>document.querySelector('#years-driving')).value;
    let milesSubmit=(<HTMLInputElement>document.querySelector('#miles')).value;

    //Calculates premiums and discounts based on inputted data
    var genderPremium=1
    if (genderSubmit == "male"){
      genderPremium=1.1
    }
    else if (genderSubmit == "female"){
      genderPremium=.9
    }
    var agePremium=1
    var ageNum = +ageSubmit
    if (ageNum <  25){
      agePremium=1.1
    }
    else if (+ageNum >  60){
      agePremium=1.1
    }
    var experiencePremium=1
    let experienceNumFormat= yearsDrivenSubmit.charAt(0)
    var experienceNum =+experienceNumFormat
    experiencePremium= 1.3 - experienceNum/10

    var weatherPremium=1
    var tempNum = +temperatureSubmit
    if (tempNum <= 0) {
      weatherPremium=1.2
    }

    let milesNum=+milesSubmit
    //Calculates final per mile and total insurance price
    let price = 1 * genderPremium * agePremium * experiencePremium *  weatherPremium * .065
    price= Math.round(100*price)/100;
    document.querySelector('#rate').innerHTML="$"+price + " per mile";
    let totalPrice= Math.round(100*(price*milesNum))/100;
    document.querySelector('#totalRate').innerHTML="Total Cost: $"+totalPrice

    let data = {
      quote: quoteSubmit,
      author: authorSubmit,
      latitude: latitudeSubmit,
      longitude: longitudeSubmit,
      weatherImage: weatherImageSubmit,
      temperature: temperatureSubmit,
      highLow: highLowSubmit,
      feelsLike: feelsLikeSubmit,
      city: citySubmit,
      humidity: humiditySubmit,
      gender: genderSubmit,
      age: ageSubmit,
      drivingExperience: yearsDrivenSubmit,
      tripLength: milesSubmit,
      costPerMile: price,
      totalCost: totalPrice
    }
    const jsonString = JSON.stringify(data)
    console.log(data)
    //var MongoClient = require('mongodb').MongoClient, format = require('util').format;

    //MongoClient.connect('mongodb://127.0.0.1:27017/test', function(err,db) {

   // if (err) throw err;
   // console.log("Connected to Database");
   // var document = {name:"David", title:"About MongoDB"};

    // insert record
  //  db.collection('test').insert(document, function(err, records) {
   //     if (err) throw err;
   //     console.log("Record added as " + records[0]._id);
   // });
//});

//Store text file


  }
}
